
var game = new Phaser.Game(800, 700, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update, render: render });

function preload() {
    game.load.image('diamond', 'picture/diamond_resized.png');
    game.load.image('bullet', 'picture/bullet.png');
    game.load.image('bullet_2', 'picture/bullet_2_resized.png');
    game.load.image('bullet_3', 'picture/bullet_3_resized.png');
    game.load.image('enemyBullet', 'picture/invader_2_resized.png');
    game.load.image('bossBullet', 'picture/enemy-bullet.png');
    game.load.spritesheet('invader', 'picture/boss_1_resized.png', 96, 96);
    game.load.spritesheet('ship', 'picture/plane.png',32,32);
    game.load.image('heart', 'picture/heart_resized.png');
    game.load.image('live', 'picture/live.png');
    game.load.spritesheet('boss', 'picture/boss_1_resized.png', 96, 96);
    game.load.spritesheet('kaboom', 'picture/explode.png', 128, 128);
    game.load.image('starfield', 'picture/background_2.jpg');
    game.load.image('background', 'picture/background2.png');
    game.load.audio('explode','audio/explosion.wav');
    game.load.audio('fire','audio/fire.mp3');
    game.load.audio('enemy_fire','audio/enemy_fire.wav');

}
var emitter;
var player;
var aliens;
var bullets;
var bulletTime = 0;
var bulletTime_2 = 0;
var bulletTime_3 = 0;
var cursors;
var fireButton;
var fire2Button;
var fire3Button;
var explosions;
var starfield;
var score = 0;
var scoreString = '';
var scoreText;
var bossLive;
var boss;
var lives;
var bossBullet;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];
var explode;
var fire;
var enemy_fire;

function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);

    //  The scrolling starfield background
    starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');
    emitter = game.add.emitter(0, 0, 100);
    emitter.makeParticles('diamond');
    emitter.gravity = 200;

    explode = game.add.audio('explode');
    fire = game.add.audio('fire');
    enemy_fire = game.add.audio('enemy_fire');

    //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);

    bullets_2 = game.add.group();
    bullets_2.enableBody = true;
    bullets_2.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_2.createMultiple(30, 'bullet_2');
    bullets_2.setAll('anchor.x', 0.5);
    bullets_2.setAll('anchor.y', 1);
    bullets_2.setAll('outOfBoundsKill', true);
    bullets_2.setAll('checkWorldBounds', true);

    bullets_3a = game.add.group();
    bullets_3a.enableBody = true;
    bullets_3a.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_3a.createMultiple(30, 'bullet_3');
    bullets_3a.setAll('anchor.x', 0.5);
    bullets_3a.setAll('anchor.y', 1);
    bullets_3a.setAll('outOfBoundsKill', true);
    bullets_3a.setAll('checkWorldBounds', true);

    bullets_3b = game.add.group();
    bullets_3b.enableBody = true;
    bullets_3b.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_3b.createMultiple(30, 'bullet_3');
    bullets_3b.setAll('anchor.x', 0.5);
    bullets_3b.setAll('anchor.y', 1);
    bullets_3b.setAll('outOfBoundsKill', true);
    bullets_3b.setAll('checkWorldBounds', true);

    bullets_3c = game.add.group();
    bullets_3c.enableBody = true;
    bullets_3c.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_3c.createMultiple(30, 'bullet_3');
    bullets_3c.setAll('anchor.x', 0.5);
    bullets_3c.setAll('anchor.y', 1);
    bullets_3c.setAll('outOfBoundsKill', true);
    bullets_3c.setAll('checkWorldBounds', true);

    // The enemy's bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(30, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    bossBullets = game.add.group();
    bossBullets.enableBody = true;
    bossBullets.physicsBodyType = Phaser.Physics.ARCADE;
    bossBullets.createMultiple(30, 'bossBullet');
    bossBullets.setAll('anchor.x', 0.5);
    bossBullets.setAll('anchor.y', 1);
    bossBullets.setAll('outOfBoundsKill', true);
    bossBullets.setAll('checkWorldBounds', true);

    //  The hero!
    player = game.add.sprite(400, 500, 'ship');
    player.anchor.setTo(0.5, 0.5);
    player.angle = 180;
    player.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
    player.play('fly');
    game.physics.enable(player, Phaser.Physics.ARCADE);

    boss = game.add.group();
    boss.enableBody = true;
    boss.physicsBodyType = Phaser.Physics.ARCADE;

    //  The baddies!
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;

    createAliens();
    createBoss();

    //  The score
    scoreString = 'Score : ';
    scoreText = game.add.text(game.world.width - 200, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

    //  Lives
    lives = game.add.group();
    game.add.text(10, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });

    bossLive = game.add.group();

    //skill
    game.add.text(10, 600, 'Press A to shoot strong bullet (CD : 5s)', { font: '17px Arial', fill: '#fff' });
    game.add.text(10, 620, 'Press S to shoot three bullet (CD : 6s)', { font: '17px Arial', fill: '#fff' });

    //  Text
    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;

    for (var i = 0; i < 5; i++) 
    {
        var life = lives.create(20 + (30 * i), 60, 'heart');
        life.anchor.setTo(0.5, 0.5);
        life.alpha = 0.4;
    }
    for (var i = 0; i < 27; i++) 
    {
        var life_boss = bossLive.create(20 + (30 * i), 550, 'live');
        life_boss.anchor.setTo(0.5, 0.5);
        life_boss.alpha = 1;
    }
    

    //  An explosion pool
    explosions = game.add.group();
    explosions.createMultiple(30, 'kaboom');
    explosions.forEach(setupInvader, this);

    //  And some controls to play the game with
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    fire2Button = game.input.keyboard.addKey(Phaser.Keyboard.A);
    fire3Button = game.input.keyboard.addKey(Phaser.Keyboard.S);

    window.onkeydown = function(event) {
        
        if (event.keyCode == 80){
            game.paused = !game.paused;
        }
        if(game.paused === false){

            stateText.visible = false;
        }
        else{

            stateText.text=" PAUSE \n Press P To Resume";
            stateText.visible = true;
        }
    }
    
}

function createAliens () {

    for (var y = 0; y < 1; y++)
    {
        for (var x = 0; x < 3; x++)
        {
            var alien = aliens.create(x * 300, y * 50, 'invader');
            alien.anchor.setTo(0.5, 0.5);
            // alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
            // alien.play('fly');
            alien.body.moves = false;
            alien.visible = false;
        }
    }

    aliens.x = 100;
    aliens.y = 100;

    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var tween = game.add.tween(aliens).to( { x: 600 }, 1500, Phaser.Easing.Linear.None, true, 0, 1000, true);

    //  When the tween loops it calls descend
    tween.onLoop.add(descend, this);
}

function createBoss () {

    var elite_m = boss.create(3 * 30, 2 * 25, 'boss');
    elite_m.anchor.setTo(0.5, 0.5);
    ///alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
    ///alien.play('fly');
    elite_m.body.moves = false;
    boss.x = 0;
    boss.y = 50;
    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var tween = game.add.tween(boss).to( { x: 600 }, 5000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    //  When the tween loops it calls descend
    tween.onLoop.add(descend, this);
    
}



function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function descend() {

    aliens.y += 10;

}

function update() {

    //  Scroll the background
    starfield.tilePosition.y += 2;

    if (player.alive)
    {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);

        if (cursors.left.isDown)
        {
            if(player.body.x >= 0) player.body.velocity.x = -200;
        }
        else if (cursors.right.isDown)
        {
            if(player.body.x <= 770) player.body.velocity.x = 200;
        }
        else if (cursors.up.isDown)
        {
            if(player.body.y >= 0) player.body.velocity.y = -200;
        }
        else if (cursors.down.isDown)
        {
            if(player.body.y <= 570) player.body.velocity.y = 200;
        }

        //  Firing?
        if (fireButton.isDown)
        {
            fireBullet();
        }
        else if (fire2Button.isDown){
            fire2Bullet();
        }
        else if (fire3Button.isDown){
            fire3Bullet();
        }

        if (game.time.now > firingTimer)
        {
            enemyFires();
            bossFires();
        }

        //  Run collision
        // game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
        // game.physics.arcade.overlap(bullets_2, aliens, collisionHandler_2, null, this);
        // game.physics.arcade.overlap(bullets_3a, aliens, collisionHandler_3, null, this);
        // game.physics.arcade.overlap(bullets_3b, aliens, collisionHandler_3, null, this);
        // game.physics.arcade.overlap(bullets_3c, aliens, collisionHandler_3, null, this);


        game.physics.arcade.overlap(bullets, boss, HitBoss, null, this);
        game.physics.arcade.overlap(bullets_2, boss, HitBoss, null, this);
        game.physics.arcade.overlap(bullets_3a, boss, HitBoss, null, this);
        game.physics.arcade.overlap(bullets_3b, boss, HitBoss, null, this);
        game.physics.arcade.overlap(bullets_3c, boss, HitBoss, null, this);
        game.physics.arcade.overlap(bullets, enemyBullets, HitEnemy, null, this);
        game.physics.arcade.overlap(bullets_2, enemyBullets, HitEnemy_2, null, this);
        game.physics.arcade.overlap(bullets_3a, enemyBullets, HitEnemy_3a, null, this);
        game.physics.arcade.overlap(bullets_3b, enemyBullets, HitEnemy_3b, null, this);
        game.physics.arcade.overlap(bullets_3c, enemyBullets, HitEnemy_3c, null, this);
        game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(bossBullets, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(aliens, player, enemyHitsPlayer, null, this);
    }

}

function render() {

    // for (var i = 0; i < aliens.length; i++)
    // {
        // game.debug.body(aliens.children[i]);
    // }

}

function collisionHandler (bullet, alien) {

    //  When a bullet hits an alien we kill them both
    bullet.kill();
    explode.play();
    // alien.kill();
    bossLive = aliens.getFirstAlive();
    if(bossLive){

        bossLive.kill();
    }

    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);

    if (aliens.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        bossBullets.callAll('kill',this);
        stateText.text = " You Won";
        stateText.visible = true;

        //the "click to restart" handler
        // game.input.onTap.addOnce(restart,this);
        window.location.href = "index.html";
    }

}

function collisionHandler_2 (bullet, alien) {

    //  When a bullet hits an alien we kill them both
    // bullet.kill();
    alien.kill();
    explode.play();

    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);

    if (aliens.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        bossBullets.callAll('kill',this);
        stateText.text = " You Won, \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        // game.input.onTap.addOnce(restart,this);
        window.location.href = "index.html";
    }

}

function collisionHandler_3 (bullet, alien) {

    //  When a bullet hits an alien we kill them both
    // bullet.kill();
    alien.kill();
    bullet.kill();
    explode.play();

    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);

    if (aliens.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        bossBullets.callAll('kill',this);
        stateText.text = " You Won, \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        // game.input.onTap.addOnce(restart,this);
        window.location.href = "index.html";
    }

}

function HitEnemy (bullet, enemyBullet) {

    //  When a bullet hits an alien we kill them both
    // bullet.kill();
    enemyBullet.kill();
    bullet.kill();
    explode.play();
    emitter.x = enemyBullet.body.x;
    emitter.y = enemyBullet.body.y;
    emitter.start(true, 4000, null, 20);

    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    // var explosion = explosions.getFirstExists(false);
    // explosion.reset(enemyBullet.body.x, enemyBullet.body.y);
    // explosion.play('kaboom', 30, false, true);


}


function HitEnemy_2 (bullet, enemyBullet) {

    //  When a bullet hits an alien we kill them both
    // bullet.kill();
    enemyBullet.kill();
    explode.play();
    emitter.x = enemyBullet.body.x;
    emitter.y = enemyBullet.body.y;
    emitter.start(true, 4000, null, 20);


    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    // var explosion = explosions.getFirstExists(false);
    // explosion.reset(enemyBullet.body.x, enemyBullet.body.y);
    // explosion.play('kaboom', 30, false, true);

}
function HitEnemy_3a (bullet, enemyBullet) {

    //  When a bullet hits an alien we kill them both
    // bullet.kill();
    enemyBullet.kill();
    bullet.kill();
    explode.play();
    emitter.x = enemyBullet.body.x;
    emitter.y = enemyBullet.body.y;
    emitter.start(true, 4000, null, 20);

    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    // var explosion = explosions.getFirstExists(false);
    // explosion.reset(enemyBullet.body.x, enemyBullet.body.y);
    // explosion.play('kaboom', 30, false, true);

}
function HitEnemy_3b (bullet, enemyBullet) {

    //  When a bullet hits an alien we kill them both
    // bullet.kill();
    enemyBullet.kill();
    bullet.kill();
    explode.play();
    emitter.x = enemyBullet.body.x;
    emitter.y = enemyBullet.body.y;
    emitter.start(true, 4000, null, 20);
    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    // var explosion = explosions.getFirstExists(false);
    // explosion.reset(enemyBullet.body.x, enemyBullet.body.y);
    // explosion.play('kaboom', 30, false, true);

}

function HitEnemy_3c (bullet, enemyBullet) {

    //  When a bullet hits an alien we kill them both
    // bullet.kill();
    enemyBullet.kill();
    bullet.kill();
    explode.play();
    emitter.x = enemyBullet.body.x;
    emitter.y = enemyBullet.body.y;
    emitter.start(true, 4000, null, 20);

    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    // var explosion = explosions.getFirstExists(false);
    // explosion.reset(enemyBullet.body.x, enemyBullet.body.y);
    // explosion.play('kaboom', 30, false, true);

}

function HitBoss(bullet, boss){

    bullet.kill();
    explode.play();
    score += 20;
    scoreText.text = scoreString + score;
    live = bossLive.getFirstAlive();
    if(live){

        live.kill();
    }
    var explosion = explosions.getFirstExists(false);
    explosion.reset(boss.body.x + 74, boss.body.y + 103);
    explosion.play('kaboom', 30, false, true);
    if (bossLive.countLiving() < 1)
    {
        ///cur_live = 5;
        score += 2200;
        scoreText.text = scoreString + score;
        boss.kill();
        enemyBullets.callAll('kill');
        // enemyBullets_2.callAll('kill',this);
        // enemyBullets_3.callAll('kill',this);
        // enemyBullets_4.callAll('kill',this);
        // aliens_2.callAll('kill');

        stateText.text=" YOU WIN!!! \n Click to restart";
        stateText.visible = true;
        window.location.href = "index.html";
        //the "click to restart" handler
        // game.input.onTap.addOnce(restart,this);
    }
}

function enemyHitsPlayer (player,bullet) {
    
    bullet.kill();
    explode.play();

    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        player.kill();
        enemyBullets.callAll('kill');

        stateText.text=" GAME OVER";
        stateText.visible = true;

        //the "click to restart" handler
        
            // if(player.body.x >= 0) player.body.velocity.x = -200;
            window.location.href = "index.html";
        
        
        // game.input.onTap.addOnce(restart,this);
    }

}

function bossFires(){

    
    bossBullet = bossBullets.getFirstExists(false);
    livingEnemies.length=0;
    boss.forEachAlive(function(alien){

        // put every living enemy in an array
        livingEnemies.push(alien);
    }); 
    if (bossBullet && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
 
        // And fire the bullet from this enemy
        bossBullet.reset(shooter.body.x+50, shooter.body.y+100);
        enemy_fire.play();

        // game.physics.arcade.moveToObject(bossBullet,player,300);
        bossBullet.body.velocity.y = 400;
        // bossBullet.body.velocity.x = -400;
        firingTimer = game.time.now + 500;
    }


}

function enemyFires () {

    //  Grab the first bullet we can from the pool
    enemyBullet = enemyBullets.getFirstExists(false);

    livingEnemies.length=0; 

    aliens.forEachAlive(function(alien){

        // put every living enemy in an array
        livingEnemies.push(alien);
    });


    if (enemyBullet && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
 
        // And fire the bullet from this enemy
        enemyBullet.reset(shooter.body.x, shooter.body.y);


        game.physics.arcade.moveToObject(enemyBullet,player,300);
        firingTimer = game.time.now + 500;
    }

}

function fireBullet () {
    
    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime + 100)
    {
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 200;
            fire.play();
        }
    }

}

function fire2Bullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime_2)
    {
        //  Grab the first bullet we can from the pool
        bullet = bullets_2.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -800;
            bulletTime_2 = game.time.now + 5000;
            fire.play();
        }
    }

}

function fire3Bullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime_3)
    {
        //  Grab the first bullet we can from the pool
        bullet_1 = bullets_3a.getFirstExists(false);
        bullet_2 = bullets_3b.getFirstExists(false);
        bullet_3 = bullets_3c.getFirstExists(false);

        if (bullet_1)
        {
            //  And fire it
            bullet_1.reset(player.x, player.y + 8);
            bullet_1.angle = -45;
            bullet_1.body.velocity.y = -400;
            bullet_1.body.velocity.x = -200;
            bullet_2.reset(player.x, player.y + 8);
            bullet_2.body.velocity.y = -400;
            bullet_2.body.velocity.x = 0;
            bullet_3.reset(player.x, player.y + 8);
            bullet_3.angle = 45;
            bullet_3.body.velocity.y = -400;
            bullet_3.body.velocity.x = 200;
            bulletTime_3 = game.time.now + 6000;
            fire.play();
        }
    }

}

function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}

function restart () {

    //  A new level starts
    
    //resets the life count
    // lives.callAll('revive');
    // //  And brings the aliens back from the dead :)
    // aliens.removeAll();
    // createAliens();

    // //revives the player
    
    // player.revive();
    // //hides the text
    // stateText.visible = false;
    score = 0;  
    scoreString = '';
    create();
    

}
