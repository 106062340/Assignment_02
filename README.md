# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer
## 106062340 鄭盛元

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|N|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
0. 基本操作
    一開始是 index.html，進去之後可以選要哪個level或是Multiplayer，關卡內可按"P"暫停，右下角有三個按鍵(previous level、next level、home)，如果是第一關就沒有previous、第三關沒有next，multiplayer只有一關。
    Start Menu -> game(可暫停) -> game over(會有短暫畫面顯示輸或贏，再直接跳轉到Start Menu)
1. Jucify mechanisms : 
    Level:
    1. level_1:
        玩家可按上下左右操作、空白鍵發射子彈、A鍵發射激光、S鍵三發子彈(皆有CD)，並且玩家有五條命。
        總共有40隻小怪飛在天上，子彈會由活著的小怪隨機發射(瞄準當時玩家位置)。每個小怪一條命。
        輸贏條件:
            贏: 40隻小怪全死
            輸: 五條命耗光
    2. level_2:
        玩家可按上下左右操作、空白鍵發射子彈(也有些微CD)、A鍵發射激光、S鍵三發子彈(皆有CD)，並且玩家有五條命。
        不斷會有小怪瞄準玩家飛來，撞到小怪扣血。每個小怪一條命。
        輸贏條件:
            贏: 得到500分
            輸: 五條命耗光
    3. level_3
        玩家可按上下左右操作、空白鍵發射子彈(也有些微CD)、A鍵發射激光、S鍵三發子彈(皆有CD)，並且玩家有五條命。
        不斷會有小怪瞄準玩家飛來，撞到小怪扣血。每個小怪一條命。
        會有一個BOSS在天上飛並發射子彈，Boss有27條命。
        輸贏條件:
            贏: 打死Boss(27條命)
            輸: 五條命耗光
    4. multiplayer(Off-line)
        基本上跟第三關一樣，只是再加一個玩家。
        玩家操作:
            Player1:上下左右移動，空白鍵發射子彈，N鍵發射激光，M鍵三發子彈。
            Player2:WASD移動，X鍵發射子彈，Z鍵發射激光，C鍵三發子彈。
        輸贏條件:
            贏: 打死Boss(27條命)
            輸: 兩名玩家五條命皆耗光
    Skill:
    1. 激光:
        會有綠色子彈射出，直到超出螢幕才會消失。
    2. 三發子彈
        會有三發紅色子彈往三個方向飛出。

2. Animations : 
    飛機:飛機會有自身的動畫(Player Animation)。

    爆炸:飛機(玩家)受到攻擊會有爆炸動畫、Boss受到攻擊也會有。

3. Particle Systems : 
    射中敵人小怪: 射中敵人小怪後會有Particle的動畫，為鑽石四散掉落。

4. Sound effects : 
    爆炸: 只要發生碰撞都有。
    
    敵人發射子彈: 音效
    
    自己發射子彈: 音效

5. Leaderboard : 沒接Firebase

6. UI : 有Score、玩家血量、Boss的話也有下方紅色血條。

7. Appearance : 看助教囉!

# Bonus Functions Description : 
1. Multiplayer(Off-line) : 有兩人線下模式。
2. Boss : 第三關有Boss，會左右移動跟發射較快子彈。
3. 暫停 : 可在遊戲中按暫停(P)。
4. 瞄準 : 敵人子彈會瞄準玩家位置(但不是導彈)。
